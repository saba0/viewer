package com.example.viewer.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.viewer.fragments.First
import com.example.viewer.fragments.Second
import com.example.viewer.fragments.Third

class ViewPagerFragmentAdapter (activity: FragmentActivity) : FragmentStateAdapter(activity) {

    override fun getItemCount() = 3

    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> First()
            1 -> Second()
            2 -> Third()
            else -> First()

        }
    }
}



