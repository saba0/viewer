package com.example.viewer.fragments


import android.content.Context.MODE_PRIVATE
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.example.viewer.R


class First : Fragment (R.layout.first) {

    private lateinit var addButton: Button
    private lateinit var noteEditText: EditText
    private lateinit var textView: TextView



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        addButton = view.findViewById(R.id.button)
        noteEditText = view.findViewById(R.id.note)
        textView = view.findViewById(R.id.textView)

        val sharedPreferences = requireActivity().getSharedPreferences("APP_PR", MODE_PRIVATE)
        val notes = sharedPreferences.getString("notes", "")
        textView.text = notes

        addButton.setOnClickListener{

            val note = noteEditText.text.toString()
            val text = textView.text.toString()
            val result = note + "\n" + text
            textView.text = result

            sharedPreferences.edit()
                .putString("notes", result)
                .apply()
        }


    }

}